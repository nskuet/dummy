<?php
namespace App\Common\Api\PayPay;

class ResponseApi
{
    const SUCCESS_RETURN_CODE = 0;
    const FAIL_RETURN_CODE = 2;

    public static function BuildSuccessResponseApiWithRtnCd($data = [])
    {
        $dataResponse = [
            "rtnCd"    => self::SUCCESS_RETURN_CODE
        ];
        $dataResponse = array_merge($dataResponse, $data);

        return response()->json($dataResponse);
    }

    public static function BuildErrorResponseApiWithRtnCd($errTitle, $errMsg)
    {
        return response()->json([
            "rtnCd" => self::FAIL_RETURN_CODE,
            "errTitle" => $errTitle,
            "errMsg" => $errMsg
        ]);
    }

    public static function BuildSuccessResponseApiWithOutRtnCd($data)
    {
        return response()->json($data);
    }

    public static function BuildErrorResponseApiWithOutRtnCd($data)
    {
        return response()->json($data);
    }


}
