<?php

namespace App\Http\Controllers\Api\PayPay;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PayPay\DeletePayPayInfoRequest;
use App\Http\Requests\Api\PayPay\PaypaySettlementRequest;
use App\Http\Requests\Api\PayPay\GetOrderHistoryDetailRequest;
use App\Http\Requests\Api\PayPay\GetPaymentInfoRequest;
use App\Common\Api\PayPay;

class OrderController extends Controller
{
    public function deletePayPayInfo(DeletePayPayInfoRequest $request)
    {
        $data = [];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }

    public function paypaySettlement(PaypaySettlementRequest $request)
    {
        $data = [];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }

    public function getOrderHistoryDetail(GetOrderHistoryDetailRequest $request)
    {
        $data = [
            "paymentType" => 3,
            "ccNo" => "123456"
        ];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }

    public function getPaymentInfo(GetPaymentInfoRequest $request)
    {
        $data = [
            "listCardRegistered" => [
                [
                    "paymentType" => 2,
                    "ccNo" => "xxxxxxxxx2301",
                    "cardNo" => "0123456789012345"
                ],
                [
                    "paymentType" => 4,
                    "paypayNo" => "*******2301",
                    "balance" => "1000"
                ]
            ]
        ];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }
}
