<?php

namespace App\Http\Controllers\Api\PayPay;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\PayPay\GetAuthorizeURLRequest;
use App\Http\Requests\Api\PayPay\GetPayPayUserInfoRequest;
use Illuminate\Http\Request;
use App\Common\Api\PayPay;

class UserController extends Controller
{
    //
    public function getAuthorizeURL(GetAuthorizeURLRequest $request)
    {
        $data = [
            "urlLogin" => "https://www.paypay.ne.jp/app/opa/user_authorization?apiKey={apiKey}&requestToken={jwtToken}"
        ];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }

    public function getPayPayUserInfo(GetPayPayUserInfoRequest $request)
    {
        $data = [
            "aud" => "<merchant organization id>",
            "iss" => "paypay.ne.jp",
            "exp" => 23567,
            "result" => "succeeded",
            "profileIdentifier" => "*******5678",
            "nonce" => "<the same nonce in the request>",
            "userAuthorizationId" => "<PayPay user reference id>",
            "referenceId" => "<merchant user reference id>"
        ];
        return PayPay\ResponseApi::BuildSuccessResponseApiWithRtnCd($data);
    }
}
