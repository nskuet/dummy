<?php

namespace App\Http\Requests\Api\PayPay;

use Illuminate\Foundation\Http\FormRequest;
use App\Common\Api\PayPay;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class GetPayPayUserInfoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'accessCode' => 'required'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(PayPay\ResponseApi::BuildErrorResponseApiWithRtnCd("項目チェックエラー", "The provided value is invalid"));
    }
}
