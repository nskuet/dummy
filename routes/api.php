<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix' => 'paypay'], function () {
    Route::post('getAuthorizeURL', ['uses' => 'Api\PayPay\UserController@getAuthorizeURL']);
    Route::post('getPayPayUserInfo', ['uses' => 'Api\PayPay\UserController@getPayPayUserInfo']);
    Route::post('deletePayPayInfo', ['uses' => 'Api\PayPay\OrderController@deletePayPayInfo']);
    Route::post('paypaySettlement', ['uses' => 'Api\PayPay\OrderController@paypaySettlement']);
    Route::post('getOrderHistoryDetail', ['uses' => 'Api\PayPay\OrderController@getOrderHistoryDetail']);
    Route::post('getPaymentInfo', ['uses' => 'Api\PayPay\OrderController@getPaymentInfo']);
});
